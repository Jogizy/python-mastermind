#!/usr/bin/env python3
# coding=UTF-8
#==============================================================================
# title           :mastermind.py
# description     :A customizable mastermind game
# author          :Olivier Meloche
# credit          :None
# date            :20200806
# version         :0.0.1
# usage           :python mastermind.py
# python_version  :3.7.1
#==============================================================================
from random import randint
from pprint import pprint

colours = ["red","orange","yellow","blue","green","brown","white","black"]
columns = 4
rows = 12

def new_board():
    l_board = [[0 for x in range(columns)] for y in range(rows)]
    return l_board

def new_choices():
    l_i = 1
    l_guessed_inputs = []
    while l_i <= columns:
        l_guessed_inputs.append(input("Guess color #"+str(l_i)+":"))
        l_i += 1
    return l_guessed_inputs

def randomize_puzzle():
    l_result = []
    for i in range(columns):
        l_result.append(colours[randint(0, len(colours)-1)])
    return l_result

def check(a_puzzle):
    l_chosen = new_choices()
    l_result = { "red": 0, "white": 0 }
    for color in colours:
        l_result["white"] += min(a_puzzle.count(color), l_chosen.count(color))
    for code_peg, guess_peg in zip(a_puzzle, l_chosen):
        if code_peg == guess_peg:
            l_result["red"] += 1
    l_result["white"] -= l_result["red"]
    return l_result, l_chosen

def update_board(a_row, a_elements):
    for i in range(columns):
        board[a_row][i] = a_elements[i]
    return board

def game(a_puzzle):
    l_isWinner = False
    l_current_row = 0
    while l_isWinner == False and l_current_row < rows:
        print("---------------------------")
        print("\t"+str(rows-l_current_row)+" try left\t")
        print("---------------------------")
        result, chosen = check(a_puzzle)
        pprint(update_board(l_current_row, chosen))
        print(result)
        if result["red"] == 4:
            l_isWinner = True
        l_current_row += 1
    return l_isWinner

def main():
    l_puzzle = randomize_puzzle()
    print("SECRET:", l_puzzle, "\n") # <-- secret
    l_game_result = game(l_puzzle)
    if l_game_result:
        print("You win!")
    else:
        print("You lose. The secret was:", l_puzzle)

board = new_board()
main()
